//
//  String+Extensions.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 22/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import Foundation

extension String {

    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }

    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
}
