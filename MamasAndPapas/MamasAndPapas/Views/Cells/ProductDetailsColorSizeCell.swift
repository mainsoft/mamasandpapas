//
//  ProductDetailsColorSizeCell.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 22/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import UIKit

class ProductDetailsColorSizeCell: UITableViewCell {

    @IBOutlet weak var lbColorTitle: UILabel!
    @IBOutlet weak var lbColor: UILabel!
    @IBOutlet weak var lbSizeTitle: UILabel!
    @IBOutlet weak var lbSize: UILabel!
    @IBOutlet weak var btSize: UIButton!
    @IBOutlet weak var btColor: UIButton!
    @IBOutlet weak var ivColorArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
