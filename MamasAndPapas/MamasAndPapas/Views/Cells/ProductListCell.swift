//
//  ProductListCell.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 22/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import UIKit

class ProductListCell: UICollectionViewCell {

    @IBOutlet weak var ivPoster: UIImageView!
    @IBOutlet weak var aiWait: UIActivityIndicatorView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubtitle: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    

}
