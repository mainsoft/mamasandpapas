//
//  ProductDetailsController.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 21/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import UIKit

enum AttributeViewType: NSInteger {
    case color = 0
    case size = 1
}

class ProductDetailsController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var ivPoster: UIImageView!
    @IBOutlet weak var aiWait: UIActivityIndicatorView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSubtitle: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
    @IBOutlet weak var lbAttributesTitle: UILabel!
    @IBOutlet weak var vwAttributes: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var vwAttribtesBackground: UIView!

    @IBOutlet weak var vwAttributesBottomConstraint: NSLayoutConstraint!

    var currentAttrtibuteViewType: AttributeViewType = .size

    var product: Product!

    lazy var viewModel: ProductDetailsViewModel = {
        return ProductDetailsViewModel(with: self.product)
    }()


    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initVM()
        setupUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        guard let headerView = tableView.tableHeaderView else { return }

        let height =  ProductDetailsConstants.headerHeight + 4 * self.view.bounds.width / 3
        var headerFrame = headerView.frame

        if height != headerFrame.size.height {
            headerFrame.size.height = height
            headerView.frame = headerFrame
            tableView.tableHeaderView = headerView

            if #available(iOS 9.0, *) {
                tableView.layoutIfNeeded()
            }
        }

        headerView.translatesAutoresizingMaskIntoConstraints = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - IBActions

    @objc func sizeButtonTapped() {
        currentAttrtibuteViewType = .size
        collectionView.reloadData()
        managesBottomView(true)
    }

    @objc func colorButtonTapped() {
        currentAttrtibuteViewType = .color
        collectionView.reloadData()
        managesBottomView(true)
    }

    @IBAction func btAttributesBackgroundTapped(_ sender: Any) {
        managesBottomView(false)
    }

    //MARK: - Custom Accessors

    // MARK: initial Ui setup
    func initView() {
        self.navigationItem.title = product.name != nil ? product.name : ""

        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
        let barButtonItemAppearance = UIBarButtonItem.appearance()
        barButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.clear], for: .normal)
    }

    // MARK: - Setup UI awith data from server
    func setupUI() {
        let dataSourceObject = viewModel.getDataModel()
        lbTitle.text = dataSourceObject.brand
        lbSubtitle.text = dataSourceObject.name
        if let price = dataSourceObject.price {
            lbPrice.text = "USD \(price)"
        } else {
            lbPrice.text = ""
        }
        ivPoster.image = nil
        if let imageSrc = viewModel.currentPosterUrl() {
            let imageUrlString = "\(NetWorkConstants.imagesBaseUrl)\(imageSrc)"
            if let imageUrl = URL(string: imageUrlString) {
                aiWait.startAnimating()
                ivPoster.sd_setImage(with: imageUrl) { (image, error, cacheType, url) in
                    self.aiWait.stopAnimating()
                }
            }
        }
        managesBottomView(false)
    }

    // MARK: Setup view model for screen, add callbacks and get data from backend
    func initVM() {
        viewModel.showAlertClosure = { [weak self] (message) in
            if let welf = self {
                DispatchQueue.main.async {
                    welf.showAlert( message )
                }
            }
        }
        viewModel.showSizeViewClosure = { [weak self] in
            if let welf = self {
                DispatchQueue.main.async {
                    welf.sizeButtonTapped()
                }
            }
        }
    }

    // MARK: Show Alert message
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: Show or hide sizes selector

    func managesBottomView(_ visible: Bool) {
        lbAttributesTitle.text = currentAttrtibuteViewType == .color ? "Choose Color" : "Choose Size"
        vwAttributesBottomConstraint.constant = visible ? 0 : -vwAttributes.bounds.size.height
        UIView.animate(withDuration: 0.3) {
            self.vwAttribtesBackground.alpha = visible ? 0.3 : 0.0
            self.view.layoutIfNeeded()
        }
    }

}

// MARK: - UITableViewDataSource

extension ProductDetailsController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {

        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.productDetailsColorSizeCell, for: indexPath) as! ProductDetailsColorSizeCell
            cell.lbColorTitle.text = ProductDetailsConstants.colorTitle
            cell.lbSizeTitle.text  = ProductDetailsConstants.sizeTitle
            cell.lbColor.text = viewModel.getColorValue()
            cell.lbSize.text = viewModel.getSizeValue()
            cell.btSize.addTarget(self, action: #selector(sizeButtonTapped), for: .touchUpInside)
            cell.btColor.addTarget(self, action: #selector(colorButtonTapped), for: .touchUpInside)
            if viewModel.isColorSelectionAvailable() {
                cell.btColor.isHidden = false
                cell.ivColorArrow.isHidden = false
            } else {
                cell.btColor.isHidden = true
                cell.ivColorArrow.isHidden = true
            }
            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.productDetailsColorActionCell, for: indexPath) as! ProductDetailsActionCell
            return cell

        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.productDetailsAttributeCell, for: indexPath) as! ProductDetailsAttributeCell
            if let attributes = viewModel.getDataModel().copyAttributes {
                cell.lbTitle.text = attributes[indexPath.row].name
                if let value = attributes[indexPath.row].value {
                    cell.tvAttribute.attributedText = value.html2AttributedString
                }

            }
            return cell

        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.productDetailsColorActionCell, for: indexPath)
            return cell

        }
    }

}

// MARK: - UITableViewDelegate

extension ProductDetailsController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.HeightForRow(in: indexPath.section)
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.HeightForRow(in: indexPath.section)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.userPressed(at: indexPath)
    }

}

//MARK: - UICollectionViewDataSource

extension ProductDetailsController: UICollectionViewDataSource {


    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if currentAttrtibuteViewType == .color {
            return viewModel.numberOfColors()
        } else {
            return viewModel.numberOfSizes()
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.productDetailsSizeCell, for: indexPath) as! ProductDetailsSizeCell
        if currentAttrtibuteViewType == .color {
            if let colorAttribute = viewModel.getColorAttribute(for: indexPath.row) {
                cell.lbTitle.text = colorAttribute.label
                if let hex = colorAttribute.hex {
                    cell.lbTitle.textColor = UIColor.hexStringToUIColor(hex: hex)
                    if viewModel.getColorValue() == colorAttribute.label {
                        cell.layer.borderColor = UIColor.hexStringToUIColor(hex: hex).cgColor
                        cell.layer.borderWidth = 1.0
                    } else {
                        cell.layer.borderColor = UIColor.clear.cgColor
                        cell.layer.borderWidth = 0.0
                    }
                }
            }

        } else {
            cell.lbTitle.text = viewModel.getSizeLabel(for: indexPath.row)
            if viewModel.selectedSize?.label != nil && cell.lbTitle.text == viewModel.selectedSize!.label {
                cell.lbTitle.textColor = UIColor.black
                cell.layer.borderColor = UIColor.black.cgColor
                cell.layer.borderWidth = 1.0
            } else {
                if viewModel.getSizeAvailability(for: indexPath.row) {
                    cell.lbTitle.textColor = UIColor.black
                    cell.layer.borderColor = UIColor.clear.cgColor
                    cell.layer.borderWidth = 0.0
                } else {
                    cell.lbTitle.textColor = UIColor.lightGray
                    cell.layer.borderColor = UIColor.clear.cgColor
                    cell.layer.borderWidth = 0.0
                }

            }
        }
        return cell
    }

}

//MARK: - UICollectionViewDelegate

extension ProductDetailsController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if currentAttrtibuteViewType == .color {
            viewModel.userPressedAtColor(at: indexPath)
            tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            setupUI()
            managesBottomView(false)
            collectionView.reloadData()
        } else {
            if viewModel.canUserSelectSize(at: indexPath) {
                viewModel.userPressedAtSize(at: indexPath)
                tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                managesBottomView(false)
                collectionView.reloadData()
            }
        }

    }

}


