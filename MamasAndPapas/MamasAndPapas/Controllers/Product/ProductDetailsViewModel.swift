//
//  ProductDetailsViewModel.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 22/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import Foundation
import UIKit

class ProductDetailsViewModel {

    private var dataModel: Product!

    private var sizesArray: [ConfigurableAttributeValue] = [ConfigurableAttributeValue]()
    private var colorsArray: [ConfigurableAttributeValue] = [ConfigurableAttributeValue]()
    var selectedSize: ConfigurableAttributeValue?
    var selectedColor: ConfigurableAttributeValue?

    var reloadTableViewClosure: (()->())?
    var showAlertClosure: ((String)->())?
    var showSizeViewClosure: (()->())?



    // MARK: - Init

    init(with product: Product) {
        self.dataModel = product
        if let attributes = product.configurableAttributes {
            if let index = attributes.index(where: {$0.name != nil && $0.name == "sizeCode"}) {
                let sizeAttribute = attributes[index]
                if let sizes = sizeAttribute.values {
                    sizesArray = sizes
                }
            }
        }
        if let attributes = product.configurableAttributes {
            if let index = attributes.index(where: {$0.name != nil && $0.name == "color"}) {
                let colorAttribute = attributes[index]
                if let colors = colorAttribute.values {
                    colorsArray = colors
                }
            }
        }
    }

    // MARK: - Work with data

    // MARK: Return current image address

    func currentPosterUrl() -> String? {
        if let imageUrl = selectedColor?.imageSrc {
            return imageUrl
        } else if let imageUrl = dataModel.imageSrc {
            return imageUrl
        }
        return nil
    }

    // MARK: Return data source object
    func getDataModel( ) -> Product {
        return dataModel
    }

    // MARK: Returns number of cells for section

    func numberOfRows(in section: NSInteger) -> NSInteger {
        var count = 0
        switch section {

        case 0:
            count = 1
            break

        case 1:
            count = 1
            break

        case 2:
            if let attributes = dataModel.copyAttributes {
                count = attributes.count
            }
            break

        default:
            break
        }
        return count
    }

    // MARK: Returns height of  cell for section

    func HeightForRow(in section: NSInteger) -> CGFloat {
        var count: CGFloat = 0.0
        switch section {

        case 0:
            count = ProductDetailsConstants.colorSizeCellHeight
            break

        case 1:
            count = ProductDetailsConstants.actionCellHeight
            break

        case 2:
            count = UITableViewAutomaticDimension
            break

        default:
            break
        }
        return count
    }

    // MARK: - Work with configurable attributes

    // MARK: Returns size label for index
    func getSizeLabel(for index: NSInteger) -> String {
        var label = ""
        if sizesArray.count > index {
            if let sizeLabel = sizesArray[index].label {
                label = sizeLabel
            }

        }
        return label
    }

    // MARK: Get Color Attribute for index
    func getColorAttribute(for index: NSInteger) -> ConfigurableAttributeValue? {
        if colorsArray.count > index {
            return colorsArray[index]

        }
        return nil
    }


    // MARK: Returns size availability for index
    func getSizeAvailability(for index: NSInteger) -> Bool {
        var isAvailable = false
        if sizesArray.count > index {
            if let isInStock = sizesArray[index].isInStock {
                isAvailable = isInStock
            }

        }
        return isAvailable
    }

    // MARK: Return color selector availability

    func isColorSelectionAvailable() -> Bool {
        return colorsArray.count > 0
    }

    // MARK: Returns sizes count in sizes array

    func numberOfSizes() -> NSInteger {
        return sizesArray.count
    }

    // MARKL Returns colors count in sizes array

    func numberOfColors() -> NSInteger {
        return colorsArray.count
    }

    // MARK: Returns current size value
    func getSizeValue() -> String {
        var size = ""
        if selectedSize?.label != nil {
            size = selectedSize!.label!
        } else {
            size = sizesArray.count > 0 ? ProductDetailsConstants.sizeDefault : ProductDetailsConstants.sizeUnspecified
        }
        return size
    }

    // MARK: Returns current color value
    func getColorValue() -> String {
        var color = ""
        if selectedColor?.label != nil {
            color = selectedColor!.label!
        } else {
            color = dataModel.color != nil ? dataModel.color! : ProductDetailsConstants.colorDefault
        }
        return color
    }

    // MARK: Check is user can select size at index

    func canUserSelectSize(at indexPath: IndexPath) -> Bool {
        var canSelect = false
        if indexPath.row < sizesArray.count {
            let size  = sizesArray[indexPath.row]
            if let sizeInStock = size.isInStock {
                canSelect = sizeInStock
            }
        }
        return canSelect
    }

    // MARK: - User Actions Handlers
    func userPressed( at indexPath: IndexPath ) {
        if indexPath.section == 1 {
            if selectedSize != nil {
                showAlertClosure?("Item added to your bag")
            } else {
                showSizeViewClosure?()
            }

        }
    }

    func userPressedAtSize(at indexPath: IndexPath) {
        if indexPath.row < sizesArray.count {
            selectedSize = sizesArray[indexPath.row]
        }
    }



    func userPressedAtColor(at indexPath: IndexPath) {
        if indexPath.row < colorsArray.count {
            selectedColor = colorsArray[indexPath.row]
            selectedSize = nil
        }
    }

}
