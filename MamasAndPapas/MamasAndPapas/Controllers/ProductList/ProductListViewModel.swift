//
//  ProductListViewModel.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 22/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import Foundation

class ProductListViewModel {

    private var cellViewModels: [Product] = [Product]()

    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    var numberOfCells: Int {
        return cellViewModels.count
    }

    var page: NSInteger = 0
    var isLastPageLoaded = false

    var selectedProduct: Product?
    var reloadTableViewClosure: (()->())?
    var insertTableViewClosures: (([IndexPath])->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?

    // MARK: - Work with data

    //MARK: Claean current data source on pull to refresh call
    func realodData() {
        cellViewModels = [Product]()
        reloadTableViewClosure?()
        page = 0
        isLastPageLoaded = false
        fetchDataPortion()
    }

    // MARK: Get portion of data from backend
    func fetchDataPortion() {
        if isLastPageLoaded == false {
            isLoading = true
            NetworkManager.sharedManager.getProductsList(with: page) { [weak self] (success, response, error) in
                if let welf = self {
                    welf.isLoading = false
                    if success {
                        if let items = response?.items {
                            welf.fillCellViewModels(with: items)
                            if let totalPages = response?.totalPages {
                                if totalPages == welf.page {
                                    welf.isLastPageLoaded = true
                                } else {
                                    welf.page = welf.page + 1
                                }
                            }
                        }
                    } else {
                        welf.showAlertClosure?()
                    }
                }
            }
        } else {
            isLoading = false
        }
    }

    // MARK: Return data source object for specific index path
    func getCellViewModel( at indexPath: IndexPath ) -> Product {
        return cellViewModels[indexPath.row]
    }


    // MARK: Fill Cell Data Array with new portion of data
    func fillCellViewModels(with products: [Product]) {
        if (cellViewModels.count == 0) {
            cellViewModels = products
            reloadTableViewClosure?()
        } else {
            cellViewModels.append(contentsOf: products)
            let indexes = Array((cellViewModels.count - (products.count))...(cellViewModels.count - 1))
            var indexPathes = [IndexPath]()
            for index in indexes {
                indexPathes.append(IndexPath(row: index, section: 0))
            }
            insertTableViewClosures!(indexPathes)
        }
    }

    // MARK: - User Actions Handlers
    func userPressed( at indexPath: IndexPath ) {
        selectedProduct = self.cellViewModels[indexPath.row]
    }
}
