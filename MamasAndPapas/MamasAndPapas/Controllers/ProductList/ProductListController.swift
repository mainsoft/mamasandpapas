//
//  ProductListController.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 21/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import UIKit
import SDWebImage

class ProductListController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    var refereshControl: UIRefreshControl!

    lazy var viewModel: ProductListViewModel = {
        return ProductListViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        initVM()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.details {
            if let vc = segue.destination as? ProductDetailsController {
                guard let selectedProduct = viewModel.selectedProduct else {
                    return
                }
                vc.product = selectedProduct
            }
        }
    }

    //MARK: - Custom Accessors

    // MARK: initial Ui setup
    func setupUI() {
        refereshControl = UIRefreshControl()
        collectionView.addSubview(refereshControl)
        refereshControl.attributedTitle =  NSAttributedString(string: "Pull to refresh")
        refereshControl.addTarget(self, action: #selector(refreshControlValueChanged), for: .valueChanged)
    }

    // MARK: Setup view model for screen, add callbacks and get data from backend
    func initVM() {

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let welf = self {
                    welf.collectionView.reloadData()
                }

            }
        }
        viewModel.insertTableViewClosures = { [weak self] (indexPathes) in
            if let welf = self {
                DispatchQueue.main.async {
                    welf.collectionView.performBatchUpdates({
                        welf.collectionView.insertItems(at: indexPathes)
                    }, completion: nil)

                }
            }
        }
        viewModel.showAlertClosure = { [weak self] () in
            if let welf = self {
                DispatchQueue.main.async {
                    welf.showAlert( "Something went wrong" )
                }
            }
        }
        viewModel.updateLoadingStatus = { [weak self] () in
            if let welf = self {
                DispatchQueue.main.async {
                    let isLoading = welf.viewModel.isLoading
                    if isLoading == false {
                        welf.refereshControl.endRefreshing()
                    }
                }
            }
        }
        viewModel.fetchDataPortion()
    }

    // MARK: Show Alert message
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: Action for pull to refresh handler
    @objc func refreshControlValueChanged() {
        viewModel.realodData()
    }

}

//MARK: - UICollectionViewDataSource

extension ProductListController: UICollectionViewDataSource {


    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.productListCell, for: indexPath) as! ProductListCell
        let cellModel = viewModel.getCellViewModel(at: indexPath)
        cell.lbTitle.text = cellModel.brand
        cell.lbSubtitle.text = cellModel.name
        if let price = cellModel.price {
            cell.lbPrice.text = "USD \(price)"
        } else {
            cell.lbPrice.text = ""
        }
        cell.ivPoster.image = nil
        if let imageSrc = cellModel.imageSrc {
            let imageUrlString = "\(NetWorkConstants.imagesBaseUrl)\(imageSrc)"
            if let imageUrl = URL(string: imageUrlString) {
                cell.aiWait.startAnimating()
                cell.ivPoster.sd_setImage(with: imageUrl) { (image, error, cacheType, url) in
                    cell.aiWait.stopAnimating()
                }
            }
        }
        return cell
    }

}

//MARK: - UICollectionViewDelegate

extension ProductListController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.size.width - ProductListConstants.collectionViewItemInset * 3) / 2
        return CGSize(width: width, height: width * 2)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: ProductListConstants.collectionViewItemInset, left: ProductListConstants.collectionViewItemInset, bottom: 0.0, right: ProductListConstants.collectionViewItemInset)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return ProductListConstants.collectionViewItemInset
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return ProductListConstants.collectionViewItemInset
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row > 0 && indexPath.row == viewModel.numberOfCells - 4 {
            viewModel.fetchDataPortion()
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        viewModel.userPressed(at: indexPath)
        performSegue(withIdentifier: SegueIdentifier.details, sender: self)
    }

}
