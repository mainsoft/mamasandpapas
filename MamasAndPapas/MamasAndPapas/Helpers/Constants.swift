//
//  Constants.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 21/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import Foundation
import UIKit

struct CellIdentifier {
    static let productListCell                      = "ProductListCell"
    static let productDetailsColorSizeCell          = "ProductDetailsColorSizeCell"
    static let productDetailsColorActionCell        = "ProductDetailsActionCell"
    static let productDetailsAttributeCell          = "ProductDetailsAttributeCell"
    static let productDetailsSizeCell               = "ProductDetailsSizeCell"
}

struct SegueIdentifier {
    static let details                              = "DetailsSegue"
}

struct NetWorkConstants {
    static let baseUrl                              = "https://www.nisnass.ae/api"
    static let searchUrl                            = "/women/clothing"
    static let productUrl                           = "product/findbyslug?slug="
    static let imagesBaseUrl                        = "https://nis-prod.atgcdn.ae/small_light(p=listing2x,of=jpg,q=70)/pub/media/catalog/product"
}

struct ProductListConstants {
    static let collectionViewItemInset: CGFloat     = 10.0
}

struct ProductDetailsConstants {
    static let colorSizeCellHeight: CGFloat         = 60.0
    static let actionCellHeight: CGFloat            = 60.0
    static let headerHeight: CGFloat                = 99.0
    static let colorTitle                           = "Color"
    static let colorDefault                         = "Unspecified"
    static let sizeTitle                            = "Size"
    static let sizeDefault                          = "Choose"
    static let sizeUnspecified                      = "Unisize"
}


