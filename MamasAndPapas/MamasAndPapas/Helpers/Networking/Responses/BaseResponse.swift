//
//  BaseResponse.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 21/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import UIKit
import ObjectMapper

class BaseResponse: Mappable {

    required init?(map: Map) {
    }

    func mapping(map: Map) {
    }

}
