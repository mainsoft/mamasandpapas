//
//  ProductListResponse.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 21/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import UIKit
import ObjectMapper

class ProductListResponse: BaseResponse {

    var items: Array<Product>?
    var totalPages: Int?

    override func mapping(map: Map) {
        items <- map["hits"]
        totalPages <- map["pagination.totalPages"]
    }

}

class Product: Mappable {

    var name: String?
    var brand: String?
    var price: Int?
    var imageSrc: String?
    var slug: String?
    var configurableAttributes: Array<ConfigurableAttribute>?
    var copyAttributes: Array<CopyAttribute>?
    var color: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        name <- map["analytics.name"]
        brand <- map["analytics.brand"]
        price <- map["price"]
        imageSrc <- map["image"]
        slug <- map["slug"]
        configurableAttributes <- map["configurableAttributes"]
        copyAttributes <- map["copyAttributes"]
        color <- map["color"]
    }

}

class ConfigurableAttribute: Mappable {

    var name: String?
    var values: Array<ConfigurableAttributeValue>?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        name <- map["code"]
        values <- map["options"]
    }

}

class ConfigurableAttributeValue: Mappable {

    var label: String?
    var isInStock: Bool?
    var hex: String?
    var imageSrc: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        label <- map["label"]
        isInStock <- map["isInStock"]
        hex <- map["attributeSpecificProperties.hex"]
        imageSrc <- map["attributeSpecificProperties.productThumbnail"]
    }
}

class CopyAttribute: Mappable {

    var name: String?
    var value: String?

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        name <- map["name"]
        value <- map["value"]
    }

}
