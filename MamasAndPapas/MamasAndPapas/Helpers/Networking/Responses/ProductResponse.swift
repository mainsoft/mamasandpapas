//
//  ProductResponse.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 22/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import UIKit
import ObjectMapper

class ProductResponse: BaseResponse {

    var product: Product?

    override func mapping(map: Map) {
        product <- map["product"]
    }

}
