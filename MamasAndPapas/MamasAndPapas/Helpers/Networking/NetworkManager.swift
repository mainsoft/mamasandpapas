//
//  NetworkManager.swift
//  MamasAndPapas
//
//  Created by Dmitry Mozyrchuk on 21/08/2018.
//  Copyright © 2018 Mainsoft. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import AlamofireNetworkActivityLogger

class NetworkManager {

    static let sharedManager = NetworkManager()

    init() {
        NetworkActivityLogger.shared.level = .debug
        NetworkActivityLogger.shared.startLogging()
    }

    //MARK: Product List Request
    func getProductsList(with page: Int, completion: @escaping (_ success: Bool, _ response: ProductListResponse?, _ error: String?) -> Void) {
        let url = URL(string: "\(NetWorkConstants.baseUrl)\(NetWorkConstants.searchUrl)/?p=\(page)?&_fields=hits,pagination")!
        Alamofire.request(url,
                          method: .get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: nil).responseObject { (response: DataResponse<ProductListResponse>) in
                            switch response.result {
                            case .success:
                                completion(true, response.result.value, nil)
                            case .failure(let error):
                                completion(false, nil, error.localizedDescription)
                            }
        }
    }

    //MARK: Product Request
    func getProduct(with productId: String, completion: @escaping (_ success: Bool, _ response: ProductResponse?, _ error: String?) -> Void) {
        let url = URL(string: "\(NetWorkConstants.baseUrl)\(NetWorkConstants.productUrl)\(productId)")!
        Alamofire.request(url,
                          method: .get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: nil).responseObject { (response: DataResponse<ProductResponse>) in
                            switch response.result {
                            case .success:
                                completion(true, response.result.value, nil)
                            case .failure(let error):
                                completion(false, nil, error.localizedDescription)
                            }
        }
    }

}
